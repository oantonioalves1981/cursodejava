/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.siscom.beans;

import br.siscom.banco.Conexao;
import br.siscom.banco.InterfaceBanco;
import br.siscom.java.Caixa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Antonio
 */
public class CaixaBean implements InterfaceBanco {
    private Caixa caixa;

    public Caixa getCaixa() {
        return caixa;
    }

    public void setCaixa(Caixa caixa) {
        this.caixa = caixa;
    }
    
    public CaixaBean(Caixa caixa){
        this.caixa = caixa;
    }

    @Override
    public boolean inserir() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "INSERT INTO caixa (cod_cliente, cod_fornecedor, cod_movimento, tipo, valor, data_caixa)"
                + " VALUES (?,?,?,?,?,?)";
        
        try{
            Connection conexao = Conexao.getConexao();
            PreparedStatement pst = conexao.prepareStatement(query);
            pst.setInt(1,caixa.getIdCliente());
            pst.setInt(2,caixa.getIdFornecedor());
            pst.setInt(3,caixa.getIdMovimento());
            pst.setString(4,caixa.getTipo());
            pst.setDouble(5,caixa.getValor());
            pst.setDate(6,caixa.getDataCaixa());
            
            pst.executeUpdate();
            return true;
            
        }catch (SQLException e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean excluir() {
         //To change body of generated methods, choose Tools | Templates.
         String query = "DELETE FROM caixa WHERE id = ?";
         
         try{
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setInt(1,caixa.getId());
            pst.executeUpdate();
            return true;
         }catch(SQLException e){
             e.printStackTrace();
             return false;
         }
    }

    @Override
    public boolean alterar() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "UPDATE caixa SET "
                +"cod_cliente = ?, cod_fornecedor = ?, "
                +"tipo = ?, valor = ?, "
                +"cod_movimento = ?, data_caixa = ? "
                +"WHERE id = ?";
        
        try{
            Connection conexao = Conexao.getConexao();
            PreparedStatement pst = conexao.prepareStatement(query);
            pst.setInt(1,caixa.getIdCliente());
            pst.setInt(2,caixa.getIdFornecedor());
            pst.setString(4,caixa.getTipo());
            pst.setDouble(5,caixa.getValor());
            pst.setInt(3,caixa.getIdMovimento());
            pst.setDate(6,caixa.getDataCaixa());
            pst.setInt(7,caixa.getId());
            pst.executeUpdate();
            return true;
            
        }catch(SQLException e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean consultar() {
       //To change body of generated methods, choose Tools | Templates.
       String query = "SELECT id, cod_cliente, cod_fornecedor, tipo, "
               +"valor, cod_movimento, data_caixa "
               +"FROM caixa WHERE id = "+caixa.getId();
       
       try{
           Connection con = Conexao.getConexao();
           Statement stm = con.createStatement();
           ResultSet rs = stm.executeQuery(query);
           rs.next();
           
           if(rs.getRow() > 0){
               caixa.setId(rs.getInt("id"));
               caixa.setIdCliente(rs.getInt("cod_cliente"));
               caixa.setIdFornecedor(rs.getInt("cod_fornecedor"));
               caixa.setTipo(rs.getString("tipo"));
               caixa.setValor(rs.getDouble("valor"));
               caixa.setIdMovimento(rs.getInt("cod_movimento"));
               caixa.setDataCaixa(rs.getDate("data_caixa"));
               return true;
           }else{
               return false;
           }
       }catch(SQLException e){
           e.printStackTrace();
           return false;
       }
    }
    
    
}
