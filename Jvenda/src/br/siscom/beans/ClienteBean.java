/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.siscom.beans;

import br.siscom.banco.Conexao;
import br.siscom.banco.InterfaceBanco;
import br.siscom.java.Cliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Antonio
 */
public class ClienteBean implements InterfaceBanco {
    private Cliente cliente;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    public ClienteBean(Cliente cli){
        this.cliente = cli;
    }

    @Override
    public boolean inserir() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "INSERT INTO CLIENTE "+
                "(nome, endereco, bairro, cidade, estado,telefone,email) "
                +"VALUES (?,?,?,?,?,?,?)";
        
        try{
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setString(1,cliente.getNome());
            pst.setString(2,cliente.getEndereco());
            pst.setString(3,cliente.getBairro());
            pst.setString(4,cliente.getCidade());
            pst.setString(5,cliente.getEstado());
            pst.setString(6,cliente.getTelefone());
            pst.setString(7,cliente.getEmail());
            pst.executeUpdate();
            return true;
        }catch(SQLException e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean excluir() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "DELETE FROM CLIENTE WHERE id = ?";
        
        try{
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setInt(1,cliente.getId());
            pst.executeUpdate();
            return true;
        }catch(SQLException e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean alterar() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "UPDATE CLIENTE SET "
                +"nome = ?, endereco = ?, bairro = ?, "
                +"cidade = ?, estado = ?, telefone = ?, email = ? "
                +"WHERE id = ?";
        
        try{
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setString(1,cliente.getNome());
            pst.setString(2,cliente.getEndereco());
            pst.setString(3,cliente.getBairro());
            pst.setString(4,cliente.getCidade());
            pst.setString(5,cliente.getEstado());
            pst.setString(6,cliente.getTelefone());
            pst.setString(7,cliente.getEmail());
            pst.setInt(8,cliente.getId());
            pst.executeUpdate();
            return true;
        }catch(SQLException e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean consultar() {
         //To change body of generated methods, choose Tools | Templates.
         String query = "SELECT id, nome, endereco, bairro, cidade, "
                 +"estado, telefone, email "
                 +"FROM CLIENTE WHERE id = "+cliente.getId();
         
         try{
             Connection con = Conexao.getConexao();
             Statement stm = con.createStatement();
             ResultSet rs = stm.executeQuery(query);
             rs.next();
             
             if(rs.getRow()>0){
                 cliente.setId(rs.getInt("id"));
                 cliente.setNome(rs.getString("nome"));
                 cliente.setEndereco(rs.getString("endereco"));
                 cliente.setBairro(rs.getString("bairro"));
                 cliente.setCidade(rs.getString("cidade"));
                 cliente.setEstado(rs.getString("estado"));
                 cliente.setTelefone(rs.getString("telefone"));
                 cliente.setEmail(rs.getString("email"));
                return true;
             }else{
                 System.out.println("Nenhum registro encontrado");
                 return false;
             }
             
             
         }catch(SQLException e){
             e.printStackTrace();
             return false;
         }
    }
    
    
    public static void main(String[] args) {
        Cliente cli = new Cliente();
        cli.setNome("Antonio Augusto Alves");
        cli.setEndereco("Rua Gen. Daltro Filho, 30 Ap 21");
        cli.setBairro("Centro");
        cli.setCidade("Sapiranga");
        cli.setEstado("RS");
        cli.setTelefone("(51) 3030-0909");
        cli.setEmail("antonio@gmail.com");
        
        ClienteBean cb = new ClienteBean(cli);
        
        if(cb.inserir()){
            System.out.println("OK");
        }else{
            System.out.println("Erro");
        }
    }
    
}
