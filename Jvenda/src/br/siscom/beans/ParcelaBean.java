/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.siscom.beans;

import br.siscom.banco.Conexao;
import br.siscom.banco.InterfaceBanco;
import br.siscom.java.Categoria;
import br.siscom.java.Parcela;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Antonio
 */
public class ParcelaBean implements InterfaceBanco {

    private Parcela parcela;

    public Parcela getParcela() {
        return parcela;
    }

    public void setParcela(Parcela parcela) {
        this.parcela = parcela;
    }
    
    

 
    
    

    public ParcelaBean(Parcela parc) {
        this.parcela = parc;
    }

    @Override
    public boolean inserir() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "INSERT INTO parcela (documento, cod_cliente, "
                + "cod_fornecedor, cod_movimento, data_vencimento, "
                + "data_pagamento, valor) VALUES (?,?,?,?,?,?,?)";

        try {
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setString(1,parcela.getDocumento());
            pst.setInt(2, parcela.getIdCliente());
            pst.setInt(3, parcela.getIdFornecedor());
            pst.setInt(4, parcela.getIdMovimento());
            pst.setDate(5,parcela.getDataVenc());
            pst.setDate(6,parcela.getDataPagto());
            pst.setDouble(7,parcela.getValor());
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean excluir() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "DELETE FROM parcela WHERE id = ?";

        try {
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setInt(1,parcela.getId());
            pst.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean alterar() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "UPDATE parcela SET "
                +"documento = ?, cod_cliente = ?, "
                + "cod_fornecedor = ?, cod_movimento, "
                + "data_vencimento = ?, data_pagamento = ?, "
                + "valor = ? "
                + "WHERE id = ?";
        try{
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setString(1,parcela.getDocumento());
            pst.setInt(2, parcela.getIdCliente());
            pst.setInt(3, parcela.getIdFornecedor());
            pst.setInt(4, parcela.getIdMovimento());
            pst.setDate(5,parcela.getDataVenc());
            pst.setDate(6,parcela.getDataPagto());
            pst.setDouble(7,parcela.getValor()); 
            pst.setInt(8,parcela.getId());
            pst.executeUpdate();
            return true;
        }catch(SQLException e){
            return false;
        }
        
    }

    @Override
    public boolean consultar() {
       
        String query = "SELECT id, documento, cod_cliente, cod_fornecedor, "
                + "cod_movimento, data_vencimento, data_pagamento, valor "
                + "FROM parcela WHERE id = "+parcela.getId();
        try{
            Connection con = Conexao.getConexao();
           Statement stm = con.createStatement();
           ResultSet rs = stm.executeQuery(query);
           rs.next();
           
           if(rs.getRow() > 0){
               parcela.setId(rs.getInt("id"));
               parcela.setDocumento(rs.getString("documento"));
               parcela.setIdCliente(rs.getInt("cod_cliente"));
               parcela.setIdFornecedor(rs.getInt("cod_fornecedor"));
               parcela.setIdMovimento(rs.getInt("cod_movimento"));
               parcela.setDataVenc(rs.getDate("data_vencimento"));
               parcela.setDataPagto(rs.getDate("data_pagamento"));
               parcela.setValor(rs.getDouble("valor"));
               return true;
           }else{
               System.out.println("Categoria inexistente");
               return false;
           }
        }catch(SQLException e){
            e.printStackTrace();
            return false;
        }
        
    }

    public static void main(String[] args) {
       
    }
}
