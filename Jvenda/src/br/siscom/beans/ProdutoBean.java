/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.siscom.beans;

import br.siscom.banco.Conexao;
import br.siscom.banco.InterfaceBanco;
import br.siscom.java.Categoria;
import br.siscom.java.Produto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Antonio
 */
public class ProdutoBean implements InterfaceBanco {

    private Produto produto;

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }
    
    

  
    
    

    public ProdutoBean(Produto produto) {
        this.produto = produto;
    }

    @Override
    public boolean inserir() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "INSERT INTO Produto (nome, descricao, quantidade, preco, cod_categoria, cod_fornecedor) "
                + "VALUES (?,?,?,?,?,?)";

        try {
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setString(1,produto.getNome());
            pst.setString(2,produto.getDescricao());
            pst.setDouble(3,produto.getQuantidade());
            pst.setDouble(4,produto.getPreco());
            pst.setInt(6, produto.getIdFornecedor());
            pst.setInt(5, produto.getIdCategoria());
            pst.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean excluir() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "DELETE FROM produto WHERE id = ?";

        try {
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setInt(1,produto.getId());
            pst.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean alterar() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "UPDATE produto SET "
                +"nome = ?, descricao = ?, quantidade = ?, "
                + "preco = ?, cod_categoria = ?, cod_fornecedor = ? "
                + "WHERE id = ?";
        try{
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setString(1,produto.getNome());
            pst.setString(2,produto.getDescricao());
            pst.setDouble(3,produto.getQuantidade());
            pst.setDouble(4,produto.getPreco());
            pst.setInt(6, produto.getIdFornecedor());
            pst.setInt(5, produto.getIdCategoria());
            pst.setInt(7,produto.getId());
            pst.executeUpdate();
            return true;
        }catch(SQLException e){
            return false;
        }
        
    }

    @Override
    public boolean consultar() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "SELECT id, nome, quantidade, "
                + "preco, descricao, cod_categoria, cod_fornecedor"
                + " FROM produto WHERE id = "+produto.getId();
        try{
            Connection con = Conexao.getConexao();
           Statement stm = con.createStatement();
           ResultSet rs = stm.executeQuery(query);
           rs.next();
           
           if(rs.getRow() > 0){
               produto.setId(rs.getInt("id"));
               produto.setNome(rs.getString("nome"));
               produto.setQuantidade(rs.getDouble("quantidade"));
               produto.setPreco(rs.getDouble("preco"));
               produto.setIdCategoria(rs.getInt("cod_categoria"));
               produto.setIdFornecedor(rs.getInt("cod_fornecedor"));
               produto.setDescricao(rs.getString("descricao"));
               return true;
           }else{
               System.out.println("Produto inexistente");
               return false;
           }
        }catch(SQLException e){
            e.printStackTrace();
            return false;
        }
        
    }

    public static void main(String[] args) {
       
    }
}
