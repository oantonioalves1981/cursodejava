/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.siscom.java;

import br.siscom.forms.FrmPrincipal;
import javax.swing.JFrame;

/**
 *
 * @author Antonio
 */
public class SisCom {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        FrmPrincipal form = new FrmPrincipal();
        form.setLocationRelativeTo(form);
        form.setExtendedState(JFrame.MAXIMIZED_BOTH);
        form.setVisible(true);
    }
    
}
