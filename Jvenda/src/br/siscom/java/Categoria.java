/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.siscom.java;

import br.siscom.banco.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author Antonio
 */
public class Categoria {
    private int id;
    private String nome;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public Categoria(){
        
    }
    
    public Categoria(int id, String nome){
        this.id = id;
        this.nome = nome;
    }

    @Override
    public String toString() {
        return this.id + " - " + this.nome;
    }
    
    public static void carregaCombo(JComboBox combo, int cod){
        String query = "SELECT id, nome from categoria order by nome";
        
        try{
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            ResultSet rs = pst.executeQuery();
            
            //Configurando o comboBox
            combo.removeAllItems();
            combo.addItem("Selecione uma categoria");
            Categoria categoria = null;
            
                while(rs.next()){
                    Categoria cat = new Categoria(rs.getInt("id"), rs.getString("nome"));
                    combo.addItem(cat);
                        if(cod == rs.getInt("id")){
                            categoria = cat;
                        }
                }
                
                if(categoria != null){
                    combo.setSelectedItem(categoria);
                }
            
        }catch(SQLException e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Erro ao carregar as categorias");
        }
    }
    
    
    
}
