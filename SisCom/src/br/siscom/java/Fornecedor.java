/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.siscom.java;

import br.siscom.banco.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author Antonio
 */
public class Fornecedor {
    private int id;
    private String empresa;
    private String endereco;
    private String bairro;
    private String cidade;
    private String estado;
    private String telefone;
    private String email;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public Fornecedor(){
        
    }
    
    public Fornecedor(int id, String empresa){
        this.id = id;
        this.empresa = empresa;
    }

    @Override
    public String toString() {
        return this.id + " - " + this.empresa;
    }
    
    public static void carregaCombo(JComboBox combo, int cod){
        String query = "SELECT id, empresa from fornecedor order by empresa";
        
        try{
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            ResultSet rs = pst.executeQuery();
            
            //Configurando o comboBox
            combo.removeAllItems();
            combo.addItem("Selecione um fornecedor");
            Fornecedor fornecedor = null;
            
                while(rs.next()){
                    Fornecedor fornec = new Fornecedor(rs.getInt("id"), rs.getString("empresa"));
                    combo.addItem(fornec);
                        if(cod == rs.getInt("id")){
                            fornecedor = fornec;
                        }
                }
                
                if(fornecedor != null){
                    combo.setSelectedItem(fornecedor);
                }
            
        }catch(SQLException e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Erro ao carregar os fornecedorres");
        }
    }
    
    
    
}
