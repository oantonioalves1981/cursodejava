/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.siscom.java;

import br.siscom.banco.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author Antonio
 */
public class Produto {
    private int id;
    private String nome;
    private String descricao;
    private double quantidade;
    private double preco;
    private int idCategoria;
    private int idFornecedor;

    public Produto() {
       //To change body of generated methods, choose Tools | Templates.
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public int getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(int idFornecedor) {
        this.idFornecedor = idFornecedor;
    }
    
  
    
    private Produto(int id, String nome, double quantidade, double preco){
        this.id = id;
        this.nome = nome;
        this.quantidade = quantidade;
        this.preco = preco;
    }

    @Override
    public String toString() {
        return this.id+" - "+this.nome;
    }
    
    public static void carregaCombo(JComboBox combo, int cod){
     String query = "SELECT id, nome, quantidade, preco from produto order by nome";
        
        try{
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            ResultSet rs = pst.executeQuery();
            
            //Configurando o comboBox
            combo.removeAllItems();
            combo.addItem("Selecione um produto");
            Produto produto = null;
            
                while(rs.next()){
                    Produto pro = new Produto(rs.getInt("id"), rs.getString("nome"), 
                            rs.getDouble("quantidade"), rs.getDouble("preco"));
                    combo.addItem(pro);
                        if(cod == rs.getInt("id")){
                            produto = pro;
                        }
                }
                
                if(produto != null){
                    combo.setSelectedItem(produto);
                }
            
        }catch(SQLException e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Erro ao carregar os produtos");
        }    
    }
    
    
}
