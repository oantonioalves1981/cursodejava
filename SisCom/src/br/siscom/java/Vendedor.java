/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.siscom.java;

import br.siscom.banco.Conexao;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author Antonio
 */
public class Vendedor {

    private int id;
    private String nome;
    private String telefone;
    private String email;
    private double salario;
    private Date dataAdmissao;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public Date getDataAdmissao() {
        return dataAdmissao;
    }

    public void setDataAdmissao(Date dataAdmissao) {
        this.dataAdmissao = dataAdmissao;
    }

    public Vendedor() {

    }

    public Vendedor(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    @Override
    public String toString() {
        return this.id +" - "+this.nome;
    }
    
    
    
    
    public static void carregaCombo(JComboBox combo, int cod) {
        String sql = "SELECT id, nome from VENDEDOR order by nome";
        try {
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            //Configurando a combo
            combo.removeAllItems();
            combo.addItem("Selecione um vendedor ");
            Vendedor vendedor = null;
            while (rs.next()) {
                Vendedor ven = new Vendedor(rs.getInt("id"), rs.getString("nome"));
                combo.addItem(ven);
                if (cod == rs.getInt("id")) {
                    vendedor = ven;
                }

            }
            if (vendedor != null) {
                combo.setSelectedItem(vendedor);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Falha ao carregar a lista de vendedores. ");

        }

    }
}
