/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.siscom.java;

import br.siscom.banco.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author Antonio
 */
public class Cliente {
    private int id;
    private String nome;
    private String telefone;
    private String email;
    private String endereco;
    private String bairro;
    private String cidade;
    private String estado;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    public Cliente(){
        
    }
    
  public Cliente(int id, String nome){
      this.id = id;
      this.nome = nome;
  }
  
  public String toString(){
      return this.id +" - "+this.nome;
  }
  
  public static void carregaCombo(JComboBox combo, int cod){
     
     String sql = "SELECT id, nome from CLIENTE order by nome";
        try {
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            //Configurando a combo
            combo.removeAllItems();
            combo.addItem("Selecione um cliente");
            Cliente cliente = null;
            while (rs.next()) {
                Cliente cli = new Cliente(rs.getInt("id"), rs.getString("nome"));
                combo.addItem(cli);
                if (cod == rs.getInt("id")) {
                    cliente = cli;
                }
                
            }
            if (cliente != null) {
                combo.setSelectedItem(cliente);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Falha ao carregar os clientes. ");
                    
        }
     
  }
    
}
