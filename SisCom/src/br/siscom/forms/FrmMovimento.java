/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.siscom.forms;

import br.siscom.banco.Conexao;
import br.siscom.beans.CaixaBean;
import br.siscom.beans.ItemBean;
import br.siscom.beans.MovimentoBean;
import br.siscom.beans.ParcelaBean;
import br.siscom.beans.ProdutoBean;
import br.siscom.java.Caixa;
import br.siscom.java.Cliente;
import br.siscom.java.Fornecedor;
import br.siscom.java.Item;
import br.siscom.java.Produto;
import br.siscom.java.Vendedor;
import br.siscom.java.Movimento;
import br.siscom.java.Parcela;
import br.siscom.utils.Utils;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Antonio
 */
public class FrmMovimento extends javax.swing.JFrame {

    //Atributos privados da classe
    private Connection conexao;
    private DefaultTableModel dtmItens, dtmParcelas;
    private double totalMovimento, valorEntrada, valorParcela;
    private int codCliente, codFornecedor, codVendedor;
    private int mov = 0;

    /**
     * Creates new form FrmMovimento
     */
    public FrmMovimento() {
        initComponents();
        Produto.carregaCombo(cbxProduto, -1);
        Cliente.carregaCombo(cbxCliente, -1);
        Fornecedor.carregaCombo(cbxFornecedor, -1);
        Vendedor.carregaCombo(cbxVendedor, -1);
        Date hoje = new Date(System.currentTimeMillis());//Configura a exibição da data atual
        txtDataHoje.setText(Utils.dateToStr(hoje));
        rbCompra.setSelected(false);
        rbVenda.setSelected(true);
        guiasMovimento.setSelectedIndex(1);
        cbxCliente.setVisible(true);
        cbxFornecedor.setVisible(false);
        //Data do primeiro vencimento
        Date primeiroVencimento = new Date(System.currentTimeMillis());
        primeiroVencimento.setDate(primeiroVencimento.getDate() + 30);//Soma 30 dias na data de hoje
        txtVencimento.setText(Utils.dateToStr(primeiroVencimento));

        //Configurando os controles das parcelas
        rbVista.setSelected(true);//selecionado
        rbPrazo.setSelected(false);//não selecionado
        txtEntrada.setEnabled(false);
        txtParcelas.setEnabled(false);
        txtVencimento.setEnabled(false);
        txtValorParcela.setEnabled(false);
        btnParcelas.setEnabled(false);

        //Configurando as tabelas do formulario JTable
        String[] col_itens = {"Id", "Produto", "Quantidade", "Preço Unit.", "Total"};
        String[] col_parcelas = {"Número da Parcela", "Data de Vencimento", "Valor da Parcela"};
        

        Object data[][] = { ,
        };
        
        tabelaItens.setModel(new DefaultTableModel(data, col_itens) {
        });
      
        tabelaParcelas.setModel(new DefaultTableModel(data, col_parcelas) {
        });

        dtmItens = (DefaultTableModel) tabelaItens.getModel();
        dtmParcelas = (DefaultTableModel) tabelaParcelas.getModel();
        
        tabelaMovimento.removeAll();
        consultarTabela();
        


    }
    
    private void consultarTabela(){
        String sql= "Select id, data_movimento, tipo, tipo_pgto, cod_cliente, cod_fornecedor, cod_vendedor, total "
                +"FROM movimento";
        
        try{
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            String[] col_movimento = {"Id","Data", "Tipo","Pagamento",
            "Cliente", "Fornecedor","Vendedor","Valor"};
            tabelaMovimento.removeAll();
            Object regs [][] = { , };
            tabelaMovimento.setModel(new DefaultTableModel(regs, col_movimento){});
            DefaultTableModel dtm = (DefaultTableModel)tabelaMovimento.getModel();
            
            
            
            while(rs.next()){
                dtm.addRow(new String []{
                    rs.getString("id"),
                    Utils.dateToStr(rs.getDate("data_movimento")),
                    rs.getString("tipo"),
                    rs.getString("tipo_pgto"),
                    rs.getString("cod_cliente"),
                    rs.getString("cod_fornecedor"),
                    rs.getString("cod_vendedor"),
                    rs.getString("total"),
                });
            }
             
        }catch(SQLException e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null,"Erro ao consultar base de dados");
        }
    }

    public void validar() throws Exception {
        //Verifica se tem produtos na tabea
        int qtde = tabelaItens.getModel().getRowCount();
        if (qtde == 0) {
            throw new Exception("Não existe nenhum produto na lista");
        }

        // valida o cliente caso seja venda
        codCliente = 0;
        if (rbVenda.isSelected()) {
            if (cbxCliente.getSelectedItem() == "Selecione um cliente") {
                throw new Exception("Selecione o Cliente");
            }
            Cliente cli = (Cliente) cbxCliente.getSelectedItem();
            codCliente = cli.getId();
        }

        //Validar fornecedor caso dor compra
        codFornecedor = 0;
        if (rbCompra.isSelected()) {
            if (cbxFornecedor.getSelectedItem() == "Selecione um fornecedor") {
                throw new Exception("Selecione o Fornecedor");
            }
            Fornecedor frn = (Fornecedor) cbxFornecedor.getSelectedItem();
            codFornecedor = frn.getId();
        }

        //Valida vendedor
        if (cbxVendedor.getSelectedItem() == "Selecione um vendedor") {
            throw new Exception("Selecione o Vendedor");
        }
        Vendedor ven = (Vendedor) cbxVendedor.getSelectedItem();
        codVendedor = ven.getId();

        //valida condição de pagamento a prazo
        if (rbPrazo.isSelected() && dtmParcelas.getRowCount() == 0) {
            throw new Exception("Você deve gerar as parcelas.");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        grpTipo = new javax.swing.ButtonGroup();
        grpPagamento = new javax.swing.ButtonGroup();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        guiasMovimento = new javax.swing.JTabbedPane();
        guiaMovimentos = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tabelaMovimento = new javax.swing.JTable();
        guiaExecutar = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtCodigo = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtDataHoje = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        rbCompra = new javax.swing.JRadioButton();
        rbVenda = new javax.swing.JRadioButton();
        jLabel4 = new javax.swing.JLabel();
        txtCodProduto = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        cbxProduto = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        txtQuantidade = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaItens = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        cbxCliente = new javax.swing.JComboBox<>();
        cbxFornecedor = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        rbVista = new javax.swing.JRadioButton();
        rbPrazo = new javax.swing.JRadioButton();
        jLabel9 = new javax.swing.JLabel();
        txtEntrada = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtParcelas = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtVencimento = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtValorParcela = new javax.swing.JTextField();
        btnParcelas = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabelaParcelas = new javax.swing.JTable();
        jLabel13 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        jButton5 = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        cbxVendedor = new javax.swing.JComboBox<>();

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable2);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("EFETUAR COMPRA E VENDA DE ARTIGOS");

        tabelaMovimento.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(tabelaMovimento);

        javax.swing.GroupLayout guiaMovimentosLayout = new javax.swing.GroupLayout(guiaMovimentos);
        guiaMovimentos.setLayout(guiaMovimentosLayout);
        guiaMovimentosLayout.setHorizontalGroup(
            guiaMovimentosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(guiaMovimentosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 1347, Short.MAX_VALUE)
                .addContainerGap())
        );
        guiaMovimentosLayout.setVerticalGroup(
            guiaMovimentosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(guiaMovimentosLayout.createSequentialGroup()
                .addGap(85, 85, 85)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 353, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        guiasMovimento.addTab("Todos os Movimentos", guiaMovimentos);

        jLabel1.setText("Código");

        jLabel2.setText("Data");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setText("Tipo de Movimento");

        grpTipo.add(rbCompra);
        rbCompra.setText("Compra");
        rbCompra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbCompraActionPerformed(evt);
            }
        });

        grpTipo.add(rbVenda);
        rbVenda.setText("Venda");
        rbVenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbVendaActionPerformed(evt);
            }
        });

        jLabel4.setText("Código do Produto");

        jLabel5.setText("Produto");

        cbxProduto.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbxProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxProdutoActionPerformed(evt);
            }
        });

        jLabel6.setText("Quantidade");

        txtQuantidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtQuantidadeActionPerformed(evt);
            }
        });

        tabelaItens.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabelaItens);

        jButton1.setText("Pesquisar Produto");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Inserir");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Remover");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel7.setText("Cliente / Fornecedor");

        cbxCliente.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        cbxFornecedor.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel8.setText("Forma de Pagamento:");

        grpPagamento.add(rbVista);
        rbVista.setText("A Vísta");
        rbVista.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbVistaActionPerformed(evt);
            }
        });

        grpPagamento.add(rbPrazo);
        rbPrazo.setText("A prazo");
        rbPrazo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbPrazoActionPerformed(evt);
            }
        });

        jLabel9.setText("Valor da Entrada");

        jLabel10.setText("Número de Parcelas");

        jLabel11.setText("Primeiro Vencimento");

        jLabel12.setText("Valor da Parcela");

        btnParcelas.setText("Gravar Parcelas");
        btnParcelas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnParcelasActionPerformed(evt);
            }
        });

        tabelaParcelas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tabelaParcelas);

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel13.setText("Total do movimento:");

        txtTotal.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N

        jButton5.setText("Finalizar Movimento");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel14.setText("Vendedor");

        cbxVendedor.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout guiaExecutarLayout = new javax.swing.GroupLayout(guiaExecutar);
        guiaExecutar.setLayout(guiaExecutarLayout);
        guiaExecutarLayout.setHorizontalGroup(
            guiaExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(guiaExecutarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(guiaExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(guiaExecutarLayout.createSequentialGroup()
                        .addGroup(guiaExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(guiaExecutarLayout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel2))
                            .addGroup(guiaExecutarLayout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCodProduto)))
                        .addGap(18, 18, 18)
                        .addGroup(guiaExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(guiaExecutarLayout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbxProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton3)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(guiaExecutarLayout.createSequentialGroup()
                                .addComponent(txtDataHoje, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rbCompra)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rbVenda)
                                .addGap(24, 24, 24)
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbxCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbxFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel14)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbxVendedor, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(guiaExecutarLayout.createSequentialGroup()
                        .addGroup(guiaExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, guiaExecutarLayout.createSequentialGroup()
                                .addGroup(guiaExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(guiaExecutarLayout.createSequentialGroup()
                                        .addComponent(jLabel8)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(rbVista)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(rbPrazo))
                                    .addGroup(guiaExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, guiaExecutarLayout.createSequentialGroup()
                                            .addComponent(btnParcelas, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(0, 0, Short.MAX_VALUE))
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, guiaExecutarLayout.createSequentialGroup()
                                            .addGroup(guiaExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel11)
                                                .addComponent(jLabel12)
                                                .addComponent(jLabel10))
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addGroup(guiaExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(txtParcelas)
                                                .addComponent(txtValorParcela)
                                                .addComponent(txtVencimento)))
                                        .addGroup(guiaExecutarLayout.createSequentialGroup()
                                            .addComponent(jLabel9)
                                            .addGap(25, 25, 25)
                                            .addComponent(txtEntrada))))
                                .addGap(40, 40, 40)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 585, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 938, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(guiaExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTotal)
                            .addGroup(guiaExecutarLayout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addGap(0, 247, Short.MAX_VALUE))
                            .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())))
        );
        guiaExecutarLayout.setVerticalGroup(
            guiaExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(guiaExecutarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(guiaExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(txtDataHoje, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(rbCompra)
                    .addComponent(rbVenda)
                    .addComponent(jLabel7)
                    .addComponent(cbxCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbxFornecedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(cbxVendedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(guiaExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtCodProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(cbxProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(txtQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(jButton3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(guiaExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(guiaExecutarLayout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton5)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(guiaExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(guiaExecutarLayout.createSequentialGroup()
                        .addGroup(guiaExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(rbVista)
                            .addComponent(rbPrazo))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(guiaExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(txtEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(guiaExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel10)
                            .addComponent(txtParcelas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(guiaExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(txtVencimento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(guiaExecutarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(txtValorParcela, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnParcelas))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        guiasMovimento.addTab("Realizar Movimento", guiaExecutar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(guiasMovimento)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(guiasMovimento)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtQuantidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtQuantidadeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQuantidadeActionPerformed

    private void rbCompraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbCompraActionPerformed
        // TODO add your handling code here:
        cbxCliente.setVisible(false);
        cbxFornecedor.setVisible(true);
    }//GEN-LAST:event_rbCompraActionPerformed

    private void rbVendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbVendaActionPerformed
        // TODO add your handling code here:
        cbxCliente.setVisible(true);
        cbxFornecedor.setVisible(false);
    }//GEN-LAST:event_rbVendaActionPerformed

    private void rbVistaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbVistaActionPerformed
        // TODO add your handling code here:
        txtEntrada.setEnabled(false);
        txtParcelas.setEnabled(false);
        txtVencimento.setEnabled(false);
        txtValorParcela.setEnabled(false);
        btnParcelas.setEnabled(false);
    }//GEN-LAST:event_rbVistaActionPerformed

    private void rbPrazoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbPrazoActionPerformed
        // TODO add your handling code here:
        txtEntrada.setEnabled(true);
        txtParcelas.setEnabled(true);
        txtVencimento.setEnabled(true);
        txtValorParcela.setEnabled(true);
        btnParcelas.setEnabled(true);
    }//GEN-LAST:event_rbPrazoActionPerformed

    private void cbxProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxProdutoActionPerformed
        // TODO add your handling code here:
        if (cbxProduto.getSelectedIndex() > 0) {
            Produto pro = (Produto) cbxProduto.getSelectedItem();
            txtCodProduto.setText(String.valueOf(pro.getId()));
        }
    }//GEN-LAST:event_cbxProdutoActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        Produto pro = new Produto();
        FrmConsultaProduto form = new FrmConsultaProduto(pro, null, true);
        form.setLocationRelativeTo(form);
        form.setVisible(true);

        if (pro.getId() > 0) {
            txtCodProduto.setText(String.valueOf(pro.getId()));
            Produto.carregaCombo(cbxProduto, pro.getId());

        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        try {
            Produto pro = (Produto) cbxProduto.getSelectedItem();
            double quantidade = Double.valueOf(txtQuantidade.getText());
            if (quantidade <= 0) {
                throw new NumberFormatException("A quantidade deve ser maior que 0 (Zero)");
            }
            double preco = pro.getPreco();
            double total = preco * quantidade;
            if ((pro.getQuantidade() >= quantidade && rbVenda.isSelected()) || rbCompra.isSelected()) {
                dtmItens.addRow(new String[]{
                    String.valueOf(pro.getId()),
                    pro.getNome(),
                    String.valueOf(quantidade),
                    String.valueOf(pro.getPreco()),
                    String.valueOf(total)
                });
                totalMovimento = totalMovimento + total;
                txtTotal.setText(String.valueOf(totalMovimento));
            }
        } catch (ClassCastException e) {
            JOptionPane.showMessageDialog(null, "Selecione um produto");
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Digite a quantidade");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro desconhecido:\n" + e.getMessage());
        }

    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:

        try {
            int linha = tabelaItens.getSelectedRow();
            int coluna = 4;
            String total = tabelaItens.getModel().getValueAt(linha, coluna).toString();
            totalMovimento = totalMovimento - Double.valueOf(total);
            txtTotal.setText(String.valueOf(totalMovimento));
            dtmItens.removeRow(linha);
        } catch (ArrayIndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "A tabela não possui nebhum registro");
        }


    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        try {
            validar();
            

            /**
             * ********************************************************************
             * Gravando o movimento
             * ********************************************************************
             */
            //mov++;
            Movimento mv = new Movimento();
            
            mv.setDataMovimento(Utils.strToDate(txtDataHoje.getText()));
            mv.setIdCliente(codCliente);
            mv.setIdFornecedor(codFornecedor);
            mv.setIdVendedor(codVendedor);
            mv.setTipo(rbCompra.isSelected() ? "C" : "V"); //Compra - Venda
            mv.setTipoPagto(rbPrazo.isSelected() ? "AP" : "AV"); // Prazo - Vista
            mv.setTotal(totalMovimento);

            MovimentoBean mb = new MovimentoBean(mv);
            mb.setMovimento(mv);
            mb.inserir();
            
            
            
             
           
            /**
             * ************************************************************************
             * Gravar valor do movimento no caixa grava se a compra ou venda é a
             * vista ou se for a prazo e tiver valor de entrada
             * ************************************************************************
             */
            if (rbVista.isSelected() || valorEntrada > 0) {
              
                Caixa cai = new Caixa();
                cai.setDataCaixa(Utils.strToDate(txtDataHoje.getText()));
                cai.setIdFornecedor(codFornecedor);
                cai.setIdCliente(codCliente);
                cai.setValor(rbVista.isSelected() ? totalMovimento : valorEntrada);
                cai.setIdMovimento(mov);
                cai.setTipo((rbCompra.isSelected()) ? "S" : "E");
                
                if(rbVenda.isSelected() && rbVista.isSelected()){
                    cai.setDescricao("Venda a vista realizada no dia "+Utils.dateToStr(cai.getDataCaixa()));
                }else if(rbVenda.isSelected() && rbPrazo.isSelected()){
                     cai.setDescricao("Venda a prazo realizada no dia "+Utils.dateToStr(cai.getDataCaixa()));
                }else if(rbCompra.isSelected() && rbVista.isSelected()){
                    cai.setDescricao("Compra a vista realizada no dia "+Utils.dateToStr(cai.getDataCaixa()));
                }else{
                    cai.setDescricao("Compra a prazo realizada no dia "+Utils.dateToStr(cai.getDataCaixa()));
                }

                CaixaBean cb = new CaixaBean(cai);
                cb.setCaixa(cai);
                cb.inserir();
            }
            //***********************************//
            //        Gravar itens               //
            //***********************************//
            String cod_produto = null;
            String qtde = null;
            String valor = null;
            Item it = new Item();
            int codItem = 1;
            for (int linha = 0; linha < tabelaItens.getModel().getRowCount(); linha++) {
                
                cod_produto = tabelaItens.getModel().getValueAt(linha, 0).toString();
                qtde = tabelaItens.getModel().getValueAt(linha, 2).toString();
                valor = tabelaItens.getModel().getValueAt(linha, 3).toString();
                it.setCodItem(codItem);
                it.setCodProduto(Integer.valueOf(cod_produto));
                it.setQuantidade(Double.valueOf(qtde));
                it.setPreco(Double.parseDouble(valor));
                it.setCodMovimento(mov);

                ItemBean ib = new ItemBean(it);
                ib.setItem(it);
                ib.inserir();
                codItem++;

                //dar baixa do estoque
                Produto pro = new Produto();
                pro.setQuantidade(Double.valueOf(qtde));
                pro.setId(Integer.valueOf(cod_produto));

                ProdutoBean prodBean = new ProdutoBean(pro);
                prodBean.setProduto(pro);
                prodBean.alterarEstoque(pro, rbVenda.isSelected() ? "V" : "C");
            }

            //Gravação das parcelas
            String doc;
            String dataVenc;
            String valorParc;
            
            for(int linha=0; linha < tabelaParcelas.getModel().getRowCount(); linha++){
                
                doc = tabelaParcelas.getModel().getValueAt(linha,0).toString();
                dataVenc = tabelaParcelas.getModel().getValueAt(linha,1).toString();
                valorParc = tabelaParcelas.getModel().getValueAt(linha,2).toString();
                
                Parcela par = new Parcela();
                par.setDataPagto(null);
                par.setDataVenc(Utils.strToDate(dataVenc));
                par.setDocumento(doc);
                par.setIdCliente(codCliente);
                par.setIdFornecedor(codFornecedor);
                par.setIdMovimento(mov);
                par.setValor(Double.valueOf(txtValorParcela.getText()));
                par.setTipo(rbVenda.isSelected()?"R":"P");
                
                
                System.out.println("doc:"+par.getDocumento());
                System.out.println("vencimento:"+ Utils.dateToStr(par.getDataVenc()));
                System.out.println("valor:"+ par.getValor());
                
                
                ParcelaBean pb = new ParcelaBean(par);
                pb.setParcela(par);
                if(pb.inserir()){
                    System.out.println("Parcela gravada");
                }else{
                    System.out.println("Erro ao gravar as parcelas");
                }
            }
            JOptionPane.showMessageDialog(null, "Operação Completa");
            consultarTabela();
            guiasMovimento.setSelectedIndex(0);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Não foi possivel concluir a transação");
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void btnParcelasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnParcelasActionPerformed
        // TODO add your handling code here:
        try {
            Utils.limparTabela(dtmParcelas);
            int qtde = Integer.valueOf(txtParcelas.getText());
            Date vencimento = Utils.strToDate(txtVencimento.getText());
            valorEntrada = Double.valueOf(txtEntrada.getText());
            double valorParcela = (totalMovimento - valorEntrada) / qtde;
            if (valorEntrada > totalMovimento) {
                throw new Exception("Total de Entrada não pode ser maior que"
                        + "o total do movimento");
            }
            for (int x = 0; x < qtde; x++) {
                dtmParcelas.addRow(new String[]{
                    String.valueOf(x + 1) + "/" + txtParcelas.getText(),
                    Utils.dateToStr(vencimento),
                    String.valueOf(valorParcela)
                });
                vencimento.setDate(vencimento.getDate() + 30);
                txtValorParcela.setText(String.valueOf(valorParcela));
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }//GEN-LAST:event_btnParcelasActionPerformed
    //fim do botao oarcelas 
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmMovimento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmMovimento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmMovimento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmMovimento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmMovimento().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnParcelas;
    private javax.swing.JComboBox<String> cbxCliente;
    private javax.swing.JComboBox<String> cbxFornecedor;
    private javax.swing.JComboBox<String> cbxProduto;
    private javax.swing.JComboBox<String> cbxVendedor;
    private javax.swing.ButtonGroup grpPagamento;
    private javax.swing.ButtonGroup grpTipo;
    private javax.swing.JPanel guiaExecutar;
    private javax.swing.JPanel guiaMovimentos;
    private javax.swing.JTabbedPane guiasMovimento;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable jTable2;
    private javax.swing.JRadioButton rbCompra;
    private javax.swing.JRadioButton rbPrazo;
    private javax.swing.JRadioButton rbVenda;
    private javax.swing.JRadioButton rbVista;
    private javax.swing.JTable tabelaItens;
    private javax.swing.JTable tabelaMovimento;
    private javax.swing.JTable tabelaParcelas;
    private javax.swing.JTextField txtCodProduto;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextField txtDataHoje;
    private javax.swing.JTextField txtEntrada;
    private javax.swing.JTextField txtParcelas;
    private javax.swing.JTextField txtQuantidade;
    private javax.swing.JTextField txtTotal;
    private javax.swing.JTextField txtValorParcela;
    private javax.swing.JTextField txtVencimento;
    // End of variables declaration//GEN-END:variables
}
