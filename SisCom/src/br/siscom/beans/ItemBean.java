/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.siscom.beans;

import br.siscom.banco.Conexao;
import br.siscom.banco.InterfaceBanco;
import br.siscom.java.Categoria;
import br.siscom.java.Item;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Antonio
 */
public class ItemBean implements InterfaceBanco {

    private Item item;

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    
    
    

    public ItemBean(Item item) {
        this.item = item;
    }

    @Override
    public boolean inserir() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "INSERT INTO item (cod_item, cod_movimento, cod_produto, quantidade, preco)"
                + " VALUES (?,?,?,?,?)";

        try {
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setInt(1,item.getCodItem());
            pst.setInt(2,item.getCodMovimento());
            pst.setInt(3,item.getCodProduto());
            pst.setDouble(4,item.getQuantidade());
            pst.setDouble(5,item.getPreco());
            pst.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean excluir() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean alterar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean consultar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
