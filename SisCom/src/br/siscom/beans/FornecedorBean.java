/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.siscom.beans;

import br.siscom.banco.Conexao;
import br.siscom.banco.InterfaceBanco;
import br.siscom.java.Fornecedor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Antonio
 */
public class FornecedorBean implements InterfaceBanco {
    private Fornecedor fornecedor;

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }
    
    
    public FornecedorBean(Fornecedor fornec){
        this.fornecedor = fornec;
    }

    @Override
    public boolean inserir() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "INSERT INTO FORNECEDOR "+
                "(empresa, endereco, bairro, cidade, estado, telefone, email) "
                +"VALUES (?,?,?,?,?,?,?)";
        
        try{
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setString(1,fornecedor.getEmpresa());
            pst.setString(2,fornecedor.getEndereco());
            pst.setString(3,fornecedor.getBairro());
            pst.setString(4,fornecedor.getCidade());
            pst.setString(5,fornecedor.getEstado());
            pst.setString(6,fornecedor.getTelefone());
            pst.setString(7,fornecedor.getEmail());
            pst.executeUpdate();
            return true;
        }catch(SQLException e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean excluir() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "DELETE FROM FORNECEDOR WHERE id = ?";
        
        try{
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setInt(1,fornecedor.getId());
            pst.executeUpdate();
            return true;
        }catch(SQLException e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean alterar() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "UPDATE FORNECEDOR SET "
                +"empresa = ?, endereco = ?, bairro = ?, "
                +"cidade = ?, estado = ?, telefone = ?, email = ? "
                +"WHERE id = ?";
        
        try{
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setString(1,fornecedor.getEmpresa());
            pst.setString(2,fornecedor.getEndereco());
            pst.setString(3,fornecedor.getBairro());
            pst.setString(4,fornecedor.getCidade());
            pst.setString(5,fornecedor.getEstado());
            pst.setString(6,fornecedor.getTelefone());
            pst.setString(7,fornecedor.getEmail());
            pst.setInt(8,fornecedor.getId());
            pst.executeUpdate();
            return true;
        }catch(SQLException e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean consultar() {
         //To change body of generated methods, choose Tools | Templates.
         String query = "SELECT id, empresa, endereco, bairro, cidade, "
                 +"estado, telefone, email "
                 +"FROM FORNECEDOR WHERE id = "+fornecedor.getId();
         
         try{
             Connection con = Conexao.getConexao();
             Statement stm = con.createStatement();
             ResultSet rs = stm.executeQuery(query);
             rs.next();
             
             if(rs.getRow()>0){
                 fornecedor.setId(rs.getInt("id"));
                 fornecedor.setEmpresa(rs.getString("empresa"));
                 fornecedor.setEndereco(rs.getString("endereco"));
                 fornecedor.setBairro(rs.getString("bairro"));
                 fornecedor.setCidade(rs.getString("cidade"));
                 fornecedor.setEstado(rs.getString("estado"));
                 fornecedor.setTelefone(rs.getString("telefone"));
                 fornecedor.setEmail(rs.getString("email"));
                return true;
             }else{
                 System.out.println("Nenhum registro encontrado");
                 return false;
             }
             
             
         }catch(SQLException e){
             e.printStackTrace();
             return false;
         }
    }
    
    
    public static void main(String[] args) {
        Fornecedor cli = new Fornecedor();
        cli.setEmpresa("Antonio Augusto Alves");
        cli.setEndereco("Rua Gen. Daltro Filho, 30 Ap 21");
        cli.setBairro("Centro");
        cli.setCidade("Sapiranga");
        cli.setEstado("RS");
        cli.setTelefone("(51) 3030-0909");
        cli.setEmail("antonio@gmail.com");
        
        FornecedorBean fb = new FornecedorBean(cli);
        
        if(fb.inserir()){
            System.out.println("OK");
        }else{
            System.out.println("Erro");
        }
    }
    
}
