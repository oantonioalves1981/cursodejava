/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.siscom.beans;

import br.siscom.banco.Conexao;
import br.siscom.banco.InterfaceBanco;
import br.siscom.java.Categoria;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Antonio
 */
public class CategoriaBean implements InterfaceBanco {

    private Categoria categoria;

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
    
    

    public CategoriaBean(Categoria cat) {
        this.categoria = cat;
    }

    @Override
    public boolean inserir() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "INSERT INTO categoria (nome) VALUES (?)";

        try {
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setString(1, categoria.getNome());
            pst.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean excluir() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "DELETE FROM categoria WHERE id = ?";

        try {
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setInt(1, categoria.getId());
            pst.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean alterar() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "UPDATE categoria SET "
                +"nome = ? WHERE id = ?";
        try{
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setString(1, categoria.getNome());
            pst.setInt(2, categoria.getId());
            pst.executeUpdate();
            return true;
        }catch(SQLException e){
            return false;
        }
        
    }

    @Override
    public boolean consultar() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "SELECT id, nome FROM categoria WHERE id = "+categoria.getId();
        try{
            Connection con = Conexao.getConexao();
           Statement stm = con.createStatement();
           ResultSet rs = stm.executeQuery(query);
           rs.next();
           
           if(rs.getRow() > 0){
               categoria.setId(rs.getInt("id"));
               categoria.setNome(rs.getString("nome"));
               return true;
           }else{
               System.out.println("Categoria inexistente");
               return false;
           }
        }catch(SQLException e){
            e.printStackTrace();
            return false;
        }
        
    }

    public static void main(String[] args) {
        Categoria cat = new Categoria();
        cat.setId(5);


        CategoriaBean cb = new CategoriaBean(cat);

        if (cb.consultar()) {
            System.out.println("Sucesso:"+cat.getNome());
        } else {
            System.out.println("Erro");
        }
    }
}
