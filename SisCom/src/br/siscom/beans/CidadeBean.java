/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.siscom.beans;

import br.siscom.banco.Conexao;
import br.siscom.banco.InterfaceBanco;
import br.siscom.java.Cidade;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Antonio
 */
public class CidadeBean implements InterfaceBanco{
    private Cidade cidade;

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }
    
    
    
    public CidadeBean(Cidade cidade){
        this.cidade = cidade;
    }

    @Override
    public boolean inserir() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "INSERT INTO CIDADE (cidade, estado) VALUES (?,?)";
        
        try{
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setString(1,cidade.getCidade());
            pst.setString(2,cidade.getEstado());
            pst.executeUpdate();
            return true;
        }catch(SQLException e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean excluir() {
         //To change body of generated methods, choose Tools | Templates.
         String query = "DELETE FROM CIDADE WHERE id = ?";
         try{
             Connection con = Conexao.getConexao();
             PreparedStatement pst = con.prepareStatement(query);
             pst.setInt(1,cidade.getId());
             pst.executeUpdate();
             return true;
         }catch(SQLException e){
            e.printStackTrace();
            return false;
         }
    }

    @Override
    public boolean alterar() {
         //To change body of generated methods, choose Tools | Templates.
         String query = "UPDATE CIDADE SET "
                 +"cidade = ?, estado = ? "
                 +"WHERE id = ?";
         
         try{
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setString(1,cidade.getCidade());
            pst.setString(2,cidade.getEstado());
            pst.setInt(3,cidade.getId());
            pst.executeUpdate();
            return true;
         }catch(SQLException e){
             e.printStackTrace();
             return false;
         }
    }

    @Override
    public boolean consultar() {
       String query = "SELECT id, cidade, estado from Cidade WHERE id = "
               +cidade.getId();
       
       try{
           Connection con = Conexao.getConexao();
           Statement stm = con.createStatement();
           ResultSet rs = stm.executeQuery(query);
           rs.next();
           
           if(rs.getRow() > 0){
               cidade.setId(rs.getInt("id"));
               cidade.setCidade(rs.getString("cidade"));
               cidade.setEstado(rs.getString("estado"));
               return true;
           }else{
               return false;
           }
      
           
       }catch(SQLException e){
           e.printStackTrace();
           return false;
       }
    }
    
    public static void main(String[] args) {
        Cidade cid = new Cidade();
        cid.setCidade("Araricá");
        cid.setEstado("RS");
        
        CidadeBean cb = new CidadeBean(cid);
        
        if(cb.inserir()){
            System.out.println("OK");
        }else{
            System.out.println("Erro");
        }
    }
    
}
