/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.siscom.beans;

import br.siscom.banco.Conexao;
import br.siscom.banco.InterfaceBanco;
import br.siscom.java.Categoria;
import br.siscom.java.Produto;
import br.siscom.java.Vendedor;
import br.siscom.utils.Utils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Antonio
 */
public class VendedorBean implements InterfaceBanco {

    private Vendedor vendedor;

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    
    
    

  
    
    

    public VendedorBean(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    @Override
    public boolean inserir() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "INSERT INTO VENDEDOR (nome, telefone, email, salario, data_admissao) "
                + "VALUES (?,?,?,?,?)";

        try {
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setString(1,vendedor.getNome());
            pst.setString(2,vendedor.getTelefone());
            pst.setString(3,vendedor.getEmail());
            pst.setDouble(4,vendedor.getSalario());
            pst.setDate(5,vendedor.getDataAdmissao());
            pst.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean excluir() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "DELETE FROM vendedor WHERE id = ?";

        try {
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setInt(1,vendedor.getId());
            pst.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean alterar() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "UPDATE vendedor SET "
                +"nome = ?, telefone = ?, email = ?,"
                + "salario = ?, data_admissao = ? "
                + "WHERE id = ?";
        try{
            Connection con = Conexao.getConexao();
            PreparedStatement pst = con.prepareStatement(query);
            pst.setString(1,vendedor.getNome());
            pst.setString(2,vendedor.getTelefone());
            pst.setString(3,vendedor.getEmail());
            pst.setDouble(4,vendedor.getSalario());
            pst.setDate(5,vendedor.getDataAdmissao());
            pst.setInt(6,vendedor.getId());
            pst.executeUpdate();
            return true;
        }catch(SQLException e){
            return false;
        }
        
    }

    @Override
    public boolean consultar() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "SELECT id, nome, telefone, email, "
                + "salario, data_admissao "
                + " FROM vendedor WHERE id = "+vendedor.getId();
        try{
            Connection con = Conexao.getConexao();
           Statement stm = con.createStatement();
           ResultSet rs = stm.executeQuery(query);
           rs.next();
           
           if(rs.getRow() > 0){
               vendedor.setId(rs.getInt("id"));
               vendedor.setNome(rs.getString("nome"));
               vendedor.setTelefone(rs.getString("telefone"));
               vendedor.setEmail(rs.getString("email"));
               vendedor.setSalario(rs.getDouble("salario"));
               vendedor.setDataAdmissao(rs.getDate("data_admissao"));
               return true;
           }else{
               System.out.println("Produto inexistente");
               return false;
           }
        }catch(SQLException e){
            e.printStackTrace();
            return false;
        }
        
    }

    public static void main(String[] args) {
        Vendedor ve = new Vendedor();
        
        ve.setNome("Luis Carlos");
        ve.setTelefone("(51) 9630-1244");
        ve.setEmail("luisc.vendas@loja.com.br");
        ve.setSalario(1500);
        ve.setDataAdmissao(Utils.strToDate("01/02/2012"));
        
        VendedorBean vb = new VendedorBean(ve);
        
        if(vb.inserir()){
            System.out.println("Ok");
        }else{
            System.out.println("Erro");
        }
       
    }
}
