/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.siscom.beans;

import br.siscom.banco.Conexao;
import br.siscom.banco.InterfaceBanco;
import br.siscom.java.Categoria;
import br.siscom.java.Item;
import br.siscom.java.Movimento;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Antonio
 */
public class MovimentoBean implements InterfaceBanco {

    private Movimento movimento;

    public Movimento getMovimento() {
        return movimento;
    }

    public void setMovimento(Movimento movimento) {
        this.movimento = movimento;
    }

    
    
    
    public MovimentoBean(){
        
    }
    public MovimentoBean(Movimento movimento) {
        this.movimento = movimento;
    }

    @Override
    public boolean inserir() {
        //To change body of generated methods, choose Tools | Templates.
        String query = "INSERT INTO movimento (id,data_movimento, tipo, tipo_pgto,"
                + " cod_cliente, cod_fornecedor, cod_vendedor, total)"
                + " VALUES (?,?,?,?,?,?,?,?)";

        try {
            int lastId = 0;
            Connection con = Conexao.getConexao();
            
            
            PreparedStatement pst = con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
            
            ResultSet rs =pst.getGeneratedKeys();
            
                if(rs.next()){
                    lastId = rs.getInt("id")+1;
                }
            pst.setInt(1, lastId);
            pst.setDate(2,movimento.getDataMovimento());
            pst.setString(3,movimento.getTipo());
            pst.setString(4,movimento.getTipoPagto());
            pst.setInt(5,movimento.getIdCliente());
            pst.setInt(6,movimento.getIdFornecedor());
            pst.setInt(7,movimento.getIdVendedor());
            pst.setDouble(8,movimento.getTotal());
            pst.executeUpdate();
            
            
            
            
            
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean excluir() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean alterar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean consultar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
