/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.siscom.banco;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Antonio
 */
public class Conexao {
    
    
    public static Connection getConexao() throws SQLException{
        String usuario = "root";
        String senha = "r#5vj@t6%";
        String driver = "com.mysql.jdbc.Driver";
        String servidor = "jdbc:mysql://localhost";
        String banco = "jvenda";
        try{
            Class.forName(driver);
            return DriverManager.getConnection(servidor+"/"+banco, usuario, senha);
        }catch(ClassNotFoundException e){
            throw new SQLException("Erro ao conectar no servidor.");
        }
    }
    
    public static void main(String[] args) throws SQLException {
        try{
            Connection conexao = Conexao.getConexao();
            System.out.println("Você está conectado ao servidor do MySQL");
        }catch(SQLException e){
            System.out.println("Erro ao conectar ao servidor:\n"
            +"Mensagem:"+e.getMessage()+"\n"
            +"Causa do erro:"+e.getCause());
        }
    }
    
}
