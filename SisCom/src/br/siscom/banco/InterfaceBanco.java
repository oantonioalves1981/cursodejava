/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.siscom.banco;

/**
 *
 * @author Antonio
 */
public interface InterfaceBanco {
    public boolean inserir();
    public boolean excluir();
    public boolean alterar();
    public boolean consultar();
}
